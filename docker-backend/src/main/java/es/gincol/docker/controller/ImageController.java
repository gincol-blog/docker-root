package es.gincol.docker.controller;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

import com.github.dockerjava.api.DockerClient;
import com.github.dockerjava.api.model.Image;
import com.github.dockerjava.api.model.SearchItem;

import es.gincol.docker.model.DockerHostApp;
import es.gincol.docker.model.ImageApp;
import es.gincol.docker.repository.DockerHostRepository;
import es.gincol.docker.utils.DockerUtils;
import io.swagger.annotations.ApiOperation;

@CrossOrigin(origins = "*", allowedHeaders = "*")
@RestController
@RequestMapping("/images")
public class ImageController {

	private static final Logger log = LoggerFactory.getLogger(ImageController.class);
	
	@Autowired
	DockerHostRepository dockerHostRespository;
	
	@ApiOperation(value="Devuelve todas las imagenes para un host docker dado")
	@GetMapping("/all/{host}")
	public @ResponseBody List<ImageApp> imagesFromDockerHost(@PathVariable("host") String host) {
		log.info("Busqueda de todas las imagenes para el host: " + host + " en bbdd");
		List<ImageApp> listaImagenes = null;
		int listaImagenesSize = 0;
		if(host != null){
			DockerHostApp dockerHostApp = dockerHostRespository.findByHost(host);
			if(dockerHostApp != null) {
				DockerClient dockerClient = null;
				try {
					//dockerhost con formato protocolo://host:puerto (tcp://192.138.99.100:2376)
					String dockerhost = DockerUtils.getHost(dockerHostApp.getProtocol(), dockerHostApp.getHost(), dockerHostApp.getPort());
					//cliente de acceso al docker host
					dockerClient = DockerUtils.getDockerClient(dockerhost, dockerHostApp.getTls());
					log.info("dockerhost: " + dockerhost);
					//lista de imagenes
					List<Image> images = dockerClient
							.listImagesCmd()
							.exec();
					listaImagenes = new ArrayList<ImageApp>();
					ImageApp ia;
					for (Image image : images) {
						ia = new ImageApp();
						ia.setId(image.getId());
						ia.setParentId(image.getParentId());
						ia.setSize(image.getSize());
						ia.setVirtualSize(image.getVirtualSize());
						if(image.getRepoTags()!=null) {
							ia.setName(image.getRepoTags()[0].split(":")[0]);
							ia.setTag(image.getRepoTags()[0].split(":")[1]);
						} else {
							ia.setName("unknow");
							ia.setTag("unknow");
						}
						listaImagenes.add(ia);
					}
					listaImagenesSize = listaImagenes.size();
				} catch(Exception ce) {
					log.error("Error lista de imagenes del hots: " + host, ce);
				} finally {
					if(dockerClient!=null) {
						try {
							dockerClient.close();
						} catch (IOException e) {
							log.error("Error cerrando conexion con docker host: " + host, e);
						}
					}
				}
			} else {
				log.info("No existe host docker: " + host + " en bdd");
			}
		}
		log.info("Se devuelve la lista de imagenes del host: " + host + ", con un tamaño de " + listaImagenesSize + " imagenes");
		return listaImagenes;
	}
	
	@ApiOperation(value="Busca las imagenes a partir del host e imageName recibidos")
	@GetMapping("/search")
	public @ResponseBody List<ImageApp> searchImages(@RequestParam String host, @RequestParam String imageName) {
		log.info("Busqueda de todas las imagenes con nombre: " + imageName + " en el host: " + host);
		int numImages = 0;
		List<ImageApp> listaImagesReturn = null;
		if(imageName != null) {
			DockerHostApp dockerHostApp = dockerHostRespository.findByHost(host);
			if(dockerHostApp != null) {
				DockerClient dockerClient = null;
				try {
					//dockerhost con formato protocolo://host:puerto (tcp://192.138.99.100:2376)
					String dockerhost = DockerUtils.getHost(dockerHostApp.getProtocol(), dockerHostApp.getHost(), dockerHostApp.getPort());
					log.info("dockerhost: " + dockerhost);
					
					//cliente de acceso al docker host
					dockerClient = DockerUtils.getDockerClient(dockerhost, dockerHostApp.getTls());
					List<SearchItem> listaImages = dockerClient.searchImagesCmd(imageName).exec();
					listaImagesReturn = new ArrayList<ImageApp>();
					ImageApp ia = null;
					for (SearchItem searchItem : listaImages) {
						ia = new ImageApp();
						ia.setName(searchItem.getName());
						ia.setOfficial(searchItem.isOfficial());
						listaImagesReturn.add(ia);
					}
					numImages = listaImagesReturn.size();
				} catch(Exception ce) {
					log.error("Error buscando imagenes con nombre: " + imageName + " en host: " + host, ce);
				} finally {
					if(dockerClient!=null) {
						try {
							dockerClient.close();
						} catch (IOException e) {
							log.error("Error cerrando conexion con docker host: " + host, e);
						}
					}
				}
			} else {
				log.info("Host docker: " + host + " no existe");
			}
	
		}
		log.info("Se han encontrado " + numImages + " con nombre: " + imageName + " en host: " + host);
		return listaImagesReturn;
	}
	
	@ApiOperation(value="Elimina todas las imagenes en estado 'dangling' para un host docker dado")
	@DeleteMapping("/deleteImageByName")
	public @ResponseBody String deleteImagesByNameFromDockerHost(@RequestParam String host, @RequestParam String imageName) {
		log.info("Borrado de la imagene con nombre " + imageName + " en el host: " + host);
		String retorno = "";
		int numImagenesElminadas = 0;
		if(host != null) {
			DockerHostApp dockerHostApp = dockerHostRespository.findByHost(host);
			if(dockerHostApp != null) {
				DockerClient dockerClient = null;
				try {
					//dockerhost con formato protocolo://host:puerto (tcp://192.138.99.100:2376)
					String dockerhost = DockerUtils.getHost(dockerHostApp.getProtocol(), dockerHostApp.getHost(), dockerHostApp.getPort());
					log.info("dockerhost: " + dockerhost);
					//cliente de acceso al docker host
					dockerClient = DockerUtils.getDockerClient(dockerhost, dockerHostApp.getTls());
					List<Image> images = dockerClient
							.listImagesCmd()
							.exec();
					
					for (Image image : images) {
						if(image.getRepoTags() != null && image.getRepoTags()[0].split(":")[0].toString().equals(imageName)) {
							dockerClient.removeImageCmd(image.getId()).withForce(true).exec();
							numImagenesElminadas++;
						}
					}
				} catch(Exception ce) {
					log.error("Error eliminando imagenes en estado dangling del hots: " + host, ce);
					retorno = "Error eliminando imagenes en estado dangling del hots: " + host;
				} finally {
					if(dockerClient!=null) {
						try {
							dockerClient.close();
						} catch (IOException e) {
							log.error("Error cerrando conexión con docker host: " + host, e);
						}
					}
				}
			} else {
				log.info("Host docker " + host + " no existe");
				retorno = "Host docker " + host + " no existe";
			}
		} else {
			log.info("Parametro 'host' no recibido");
			retorno = "Parametro 'host' no recibido";
		}
		log.info("Se han borrado " + numImagenesElminadas + " imagenes en estado dangling");
		retorno = "Se han borrado " + numImagenesElminadas + " imagenes en estado dangling";
		return retorno;
	}
	
	@ApiOperation(value="Elimina todas las imagenes en estado 'dangling' para un host docker dado")
	@DeleteMapping("/deleteDangling/{host}")
	public @ResponseBody String deleteImagesDanglingFromDockerHost(@PathVariable("host") String host) {
		log.info("Borrado de todas las imagenes en estado dangling para el host: " + host);
		String retorno = "";
		int numImagenesElminadas = 0;
		if(host != null) {
			DockerHostApp dockerHostApp = dockerHostRespository.findByHost(host);
			if(dockerHostApp != null) {
				DockerClient dockerClient = null;
				try {
					//dockerhost con formato protocolo://host:puerto (tcp://192.138.99.100:2376)
					String dockerhost = DockerUtils.getHost(dockerHostApp.getProtocol(), dockerHostApp.getHost(), dockerHostApp.getPort());
					log.info("dockerhost: " + dockerhost);
					//cliente de acceso al docker host
					dockerClient = DockerUtils.getDockerClient(dockerhost, dockerHostApp.getTls());
					List<Image> images = dockerClient
							.listImagesCmd()
							.exec();
					
					for (Image image : images) {
						if(image.getRepoTags() == null || image.getRepoTags()[0].toString().contains("none")) {
							dockerClient.removeImageCmd(image.getId()).withForce(true).exec();
							numImagenesElminadas++;
						}
					}
				} catch(Exception ce) {
					log.error("Error eliminando imagenes en estado dangling del hots: " + host, ce);
					retorno = "Error eliminando imagenes en estado dangling del hots: " + host;
				} finally {
					if(dockerClient!=null) {
						try {
							dockerClient.close();
						} catch (IOException e) {
							log.error("Error cerrando conexión con docker host: " + host, e);
						}
					}
				}
			} else {
				log.info("Host docker " + host + " no existe");
				retorno = "Host docker " + host + " no existe";
			}
		} else {
			log.info("Parametro 'host' no recibido");
			retorno = "Parametro 'host' no recibido";
		}
		log.info("Se han borrado " + numImagenesElminadas + " imagenes en estado dangling");
		retorno = "Se han borrado " + numImagenesElminadas + " imagenes en estado dangling";
		return retorno;
	}
	
}
