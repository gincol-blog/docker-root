package es.gincol.docker.controller;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

import es.gincol.docker.model.User;
import es.gincol.docker.repository.UserRepository;
import io.swagger.annotations.ApiOperation;

@CrossOrigin(origins = "*", allowedHeaders = "*")
@RestController
@RequestMapping("/login")
public class LoginController {

	private static final Logger log = LoggerFactory.getLogger(LoginController.class);
	
	@Autowired
	UserRepository userRepository;
	
	@ApiOperation(value="Devuelve user si existe en bdd con las credenciales suministradas")
	@GetMapping()
	public @ResponseBody User allDockerHosts(@RequestParam String username, @RequestParam String password) {
		log.info("Busqueda del usuario ('"+username+"','"+password+"') en bbdd");
		User user = userRepository.findByUsernameAndPassword(username, password);
		return user;
	}
	
}
