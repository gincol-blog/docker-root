import { Injectable, EventEmitter } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { environment } from '../../environments/environment';
import { Dockerhost } from '../model/dockerhost';

const httpOptions = {
  headers: new HttpHeaders({ 'Content-Type': 'application/json' })
};

@Injectable({
  providedIn: 'root'
})
export class DockerHostService {
  
  private dockerHostUrl = environment.dockerHostUrl;
  dockerHostSelected = new EventEmitter<Dockerhost>();
  
  constructor(private http: HttpClient) { }

  public getDockerHosts() {
    return this.http.get<Dockerhost[]>(this.dockerHostUrl + "/all", httpOptions);
  }

}
