import { Component, OnInit, Input } from '@angular/core';
import { Dockerhost } from '../../model/dockerhost';

@Component({
  selector: 'app-docker-host-detail',
  templateUrl: './docker-host-detail.component.html',
  styleUrls: ['./docker-host-detail.component.css']
})
export class DockerHostDetailComponent implements OnInit {
  @Input() dockerHost: Dockerhost;

  constructor() { }

  ngOnInit() {
  }

}
