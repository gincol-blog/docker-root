package es.gincol.docker.controller;

import java.io.IOException;
import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

import com.github.dockerjava.api.DockerClient;
import com.github.dockerjava.api.model.Info;

import es.gincol.docker.model.DockerHostApp;
import es.gincol.docker.repository.DockerHostRepository;
import es.gincol.docker.utils.DockerUtils;
import io.swagger.annotations.ApiOperation;

@CrossOrigin(origins = "*", allowedHeaders = "*")
@RestController
@RequestMapping("/dockerhost")
public class DockerHostController {
	
	private static final Logger log = LoggerFactory.getLogger(DockerHostController.class);

	@Autowired
	DockerHostRepository machineRepository;
	@Autowired
	DockerHostRepository dockerHostRespository;

	@ApiOperation(value="Devuelve la lista de hosts docker de bbdd")
	@GetMapping("/all")
	public @ResponseBody List<DockerHostApp> allDockerHosts() {
		log.info("Busqueda de todos los host docker en bbdd");
		return machineRepository.findAll();
	}
	
	@ApiOperation(value="Devuelve la lista de hosts docker de bbdd")
	@GetMapping("/info/{host}")
	public @ResponseBody String dockerInfo(@PathVariable("host") String host) {
		log.info("Inicio docker info del host docker: " + host);
		String dockerInfo = "";
		if(host != null){
			DockerHostApp dockerHostApp = dockerHostRespository.findByHost(host);
			if(dockerHostApp != null) {
				DockerClient dockerClient = null;
				try {
					//dockerhost con formato protocolo://host:puerto (tcp://192.138.99.100:2376)
					String dockerhost = DockerUtils.getHost(dockerHostApp.getProtocol(), dockerHostApp.getHost(), dockerHostApp.getPort());
					log.info("dockerhost: " + dockerhost);
					
					//cliente de acceso al docker host
					dockerClient = DockerUtils.getDockerClient(dockerhost, dockerHostApp.getTls());
					
					Info info = dockerClient.infoCmd().exec();
					dockerInfo = info.toString();
				} catch(Exception ce) {
					log.error("Error estado del docker host " + host, ce);
				} finally {
					if(dockerClient!=null) {
						try {
							dockerClient.close();
						} catch (IOException e) {
							log.error("Error cerrando conexión con docker host " + host, e);
						}
					}
				}
			} else {
				log.info("No existe host docker " + host + " en bdd");
			}
		}
		log.info("Fin docker info del host: " + host + ":" + dockerInfo);
		return dockerInfo;
	}
	
	@ApiOperation(value="Devuelve el estado 'Running/Stopped' de un docker host")
	@GetMapping("/state/{host}")
	public @ResponseBody String getDockerHostsState(@PathVariable("host") String host) {
		log.info("Busqueda del estado del docker host: " + host);
		String estado = "Stopped";
		if(host != null){
			DockerHostApp dockerHostApp = dockerHostRespository.findByHost(host);
			if(dockerHostApp != null) {
				DockerClient dockerClient = null;
				try {
					//dockerhost con formato protocolo://host:puerto (tcp://192.138.99.100:2376)
					String dockerhost = DockerUtils.getHost(dockerHostApp.getProtocol(), dockerHostApp.getHost(), dockerHostApp.getPort());
					
					//cliente de acceso al docker host
					dockerClient = DockerUtils.getDockerClient(dockerhost, dockerHostApp.getTls());
					Info info = dockerClient.infoCmd().exec();
					
					//Si no salta excepcion el docker host esta levantado (Running)
					estado = "Running";
				} catch(Exception ce) {
					log.error("Error estado del docker host: " + host, ce);
				} finally {
					if(dockerClient!=null) {
						try {
							dockerClient.close();
						} catch (IOException e) {
							log.error("Error cerrando conexión con docker host: " + host, e);
						}
					}
				}
			} else {
				log.info("No existe host docker: " + host + " en bdd");
			}
		}
		log.info("Se devuelve el estado: " + estado + " del docker host: " + host);
		return estado;
	}
}
