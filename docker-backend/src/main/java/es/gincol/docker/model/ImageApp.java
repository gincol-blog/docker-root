package es.gincol.docker.model;

import java.util.Date;

public class ImageApp {
	private String id;
	private String parentId;
	private String name;
	private String tag;
	private Date created;
	private long size;
	private long virtualSize;
	private boolean official;
	
	public ImageApp() {
		super();
	}

	public ImageApp(String id, String parentId, String name, String tag, Date created, long size, long virtualSize, boolean official) {
		super();
		this.id = id;
		this.parentId = parentId;
		this.name = name;
		this.tag = tag;
		this.created = created;
		this.size = size;
		this.virtualSize = virtualSize;
		this.official = official;
	}

	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}

	public String getParentId() {
		return parentId;
	}

	public void setParentId(String parentId) {
		this.parentId = parentId;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getTag() {
		return tag;
	}

	public void setTag(String tag) {
		this.tag = tag;
	}

	public Date getCreated() {
		return created;
	}

	public void setCreated(Date created) {
		this.created = created;
	}

	public long getSize() {
		return size;
	}

	public void setSize(long size) {
		this.size = size;
	}

	public long getVirtualSize() {
		return virtualSize;
	}

	public void setVirtualSize(long virtualSize) {
		this.virtualSize = virtualSize;
	}

	public boolean isOfficial() {
		return official;
	}

	public void setOfficial(boolean official) {
		this.official = official;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((created == null) ? 0 : created.hashCode());
		result = prime * result + ((id == null) ? 0 : id.hashCode());
		result = prime * result + ((name == null) ? 0 : name.hashCode());
		result = prime * result + ((parentId == null) ? 0 : parentId.hashCode());
		result = prime * result + (int) (size ^ (size >>> 32));
		result = prime * result + ((tag == null) ? 0 : tag.hashCode());
		result = prime * result + (int) (virtualSize ^ (virtualSize >>> 32));
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		ImageApp other = (ImageApp) obj;
		if (created == null) {
			if (other.created != null)
				return false;
		} else if (!created.equals(other.created))
			return false;
		if (id == null) {
			if (other.id != null)
				return false;
		} else if (!id.equals(other.id))
			return false;
		if (name == null) {
			if (other.name != null)
				return false;
		} else if (!name.equals(other.name))
			return false;
		if (parentId == null) {
			if (other.parentId != null)
				return false;
		} else if (!parentId.equals(other.parentId))
			return false;
		if (size != other.size)
			return false;
		if (tag == null) {
			if (other.tag != null)
				return false;
		} else if (!tag.equals(other.tag))
			return false;
		if (virtualSize != other.virtualSize)
			return false;
		return true;
	}

	@Override
	public String toString() {
		return "ImageApp [id=" + id + ", parentId=" + parentId + ", name=" + name + ", tag=" + tag + ", created="
				+ created + ", size=" + size + ", virtualSize=" + virtualSize + ", official=" + official + "]";
	}

}
