import { Component, OnInit, Output, EventEmitter } from '@angular/core';
import { LoginService } from '../services/login.service';

@Component({
  selector: 'app-header',
  templateUrl: './header.component.html',
  styleUrls: ['./header.component.css']
})
export class HeaderComponent implements OnInit {

  constructor(private loginServide: LoginService) { }

  ngOnInit() {
  }

  public loggedIn(){
    return this.loginServide.loggedIn();
  }

  public logout() {
   this.loginServide.logout();
  }
}
