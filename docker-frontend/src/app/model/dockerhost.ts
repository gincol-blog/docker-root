export class Dockerhost {
    public id: string;
    public name: string;
    public protocol: string;
    public host: string;
    public port: string;
    public tls: string;

    constructor(id: string, name: string, protocol: string, host: string, port: string, tls: string){
        this.id = id;
        this.name = name;
        this.protocol = protocol;
        this.host = host;
        this.port = port;
        this.tls = tls;
    }
}
