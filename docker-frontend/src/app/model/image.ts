export class Image {
    public id: string;
    public parentId: string;
    public name: string;
    public tag: string;
    public created: Date;
    public size: number;
    public virtualSize: number;
    public official: boolean;

    constructor(id: string, parentId: string, name: string, tag: string, created: Date, size: number, virtualSize: number, official: boolean){
        this.id = id;
        this.parentId = parentId;
        this.name = name;
        this.tag = tag;
        this.created = created;
        this.size = size;
        this.virtualSize = virtualSize;
        this.official = official;
    }
}
