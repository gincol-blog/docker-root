export const environment = {
  production: false,
  envName: 'dev',
  dockerHostUrl: 'http://192.168.99.100:9090/dockerhost',
  loginUrl: 'http://192.168.99.100:9090/login',
  addUserUrl: 'http://192.168.99.100:9090/users/addShort'
};
