import { Component, OnInit, Input } from '@angular/core';
import { Dockerhost } from '../../model/dockerhost';
import { DockerHostService } from '../../services/docker-host.service';

@Component({
  selector: 'app-docker-host-item',
  templateUrl: './docker-host-item.component.html',
  styleUrls: ['./docker-host-item.component.css']
})
export class DockerHostItemComponent implements OnInit {

  @Input() dockerHost: Dockerhost;

  constructor(private dockerHostService: DockerHostService) { }

  ngOnInit() {
  }

  dockerHostSelected(){
    this.dockerHostService.dockerHostSelected.emit(this.dockerHost);
  }
}
