import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { environment } from '../../environments/environment';
import { User } from '../model/user';
import { Observable } from '../../../node_modules/rxjs';
import { Mensaje } from '../model/mensaje';

const httpOptions = {
  headers: new HttpHeaders({ 'Content-Type': 'application/json' })
};

@Injectable({
  providedIn: 'root'
})
export class UsersService {
  private addUserUrl = environment.addUserUrl;
  user: User;
  
  constructor(private http: HttpClient) { }

  public signUp(user: User) {
    return this.http.put<Mensaje>(this.addUserUrl + "?username=" + user.username + "&password=" + user.password, null, httpOptions);
  }


}
