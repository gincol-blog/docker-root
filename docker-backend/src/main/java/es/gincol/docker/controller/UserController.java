package es.gincol.docker.controller;

import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

import es.gincol.docker.model.Mensaje;
import es.gincol.docker.model.User;
import es.gincol.docker.repository.UserRepository;
import io.swagger.annotations.ApiOperation;

@CrossOrigin(origins = "*", allowedHeaders = "*")
@RestController
@RequestMapping("/users")
public class UserController {

	private static final Logger log = LoggerFactory.getLogger(UserController.class);

	@Autowired
	UserRepository userRepository;

	@ApiOperation(value="Devuelve todos los usuarios")
	@GetMapping(path = "/all")
	public @ResponseBody List<User> getAllUsers() {
		log.info("Busqueda de todos los usuarios");
		return userRepository.findAll();
	}

	@ApiOperation(value="Devuelve el usuario asociado al username recibido")
	@GetMapping(path = "/{username}")
	public @ResponseBody User getUser(@PathVariable("username") String username) {
		log.info("Busqueda del usuario " + username);
		return userRepository.findByUsername(username);
	}

	@ApiOperation(value="Inserta un usuario (o lo modifica si username ya existe) a partir de los datos recibidos. NO requiere id")
	@PutMapping(path = "/addShort")
	public @ResponseBody Mensaje addUser(@RequestParam String username, @RequestParam String password) {
		log.info("Insertando o modificando usuario con username: " + username);
		Mensaje mensaje = new Mensaje();
		User user = userRepository.findByUsername(username);
		if(user!=null) {
			user.setPassword(password);
			mensaje.setTipo("Error");
			mensaje.setTexto("Usuario ya existe");
		} else {
			user = new User(username, password);
			mensaje.setTipo("Ok");
			mensaje.setTexto("Usuario " + username + " insertado");
			userRepository.save(user);
		}
		
		log.info(mensaje.getTexto());
		return mensaje;
	}
	
	@ApiOperation(value="Inserta un usuario (o lo modifica si username ya existe) a partir de los datos recibidos. SI requiere id")
	@PutMapping(path = "/addFull")
	public @ResponseBody String addUser(@RequestParam String id, @RequestParam String username, @RequestParam String password) {
		log.info("Insertando o modificando usuario con username: " + username);
		User user = new User(id,username,password);
		userRepository.save(user);
		log.info("Usuario " + username + " insertado/modificado");
		return "Usuario " + username + " insertado/modificado";
	}

	@ApiOperation(value="Elimina el usuario asociado al id recibido")
	@DeleteMapping(path = "/delete/{id}")
	public @ResponseBody String deleteUser(@PathVariable("id") String id) {
		log.info("Elimina usuario con id: " + id);
		userRepository.deleteById(id);
		log.info("Usuario eliminado");
		return "Usuario eliminado";
	}

}
