import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { environment } from '../../environments/environment';
import { User } from '../model/user';
import { Observable } from '../../../node_modules/rxjs';

const httpOptions = {
  headers: new HttpHeaders({ 'Content-Type': 'application/json' })
};

@Injectable({
  providedIn: 'root'
})
export class LoginService {

  private loginUrl = environment.loginUrl;
  user: Observable<User>;
  logged: boolean = false;

  constructor(private http: HttpClient) { }

  public login(username: string, password: string) {
    this.user = this.http.get<User>(this.loginUrl + "?username=" + username + "&password=" + password, httpOptions);

    if(this.user){
      this.logged = true;
    } else {
      this.logged = false;
    }
    return this.user;
  }

  public loggedIn(){
    return this.logged;
  }

  public logout(){
    this.logged = false;
  }

}
