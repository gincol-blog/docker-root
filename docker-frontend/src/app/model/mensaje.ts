export class Mensaje {
    public tipo: string;
    public texto: string;

    constructor(tipo: string, texto: string){
        this.tipo = tipo;
        this.texto = texto;
    }

}
