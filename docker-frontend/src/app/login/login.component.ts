import { Component, OnInit, ViewChild, ElementRef } from '@angular/core';
import { Router, ActivatedRoute } from '@angular/router';
import { LoginService } from '../services/login.service';
import { User } from '../model/user';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.css']
})
export class LoginComponent implements OnInit {

  user: User;
  allowLogin = false;
  showError = false;

  @ViewChild('usernameInput') usernameInput: ElementRef;
  @ViewChild('passwordInput') passwordInput: ElementRef;

  constructor(private router: Router,
    private loginService: LoginService
  ) { }

  ngOnInit() {
    // reset login status
    this.loginService.logout();
  }

  login() {
    this.loginService.login(this.usernameInput.nativeElement.value, this.passwordInput.nativeElement.value)
      .subscribe(data => {
        if (data){
          this.router.navigate(['/dockerhost']);
        } else {
          this.showError = true;
        }
    });
  }

  enableLogin() {
    if (this.usernameInput.nativeElement.value && this.passwordInput.nativeElement.value){
      this.allowLogin = true;
    } else {
      this.allowLogin = false;
    }
  }

}
