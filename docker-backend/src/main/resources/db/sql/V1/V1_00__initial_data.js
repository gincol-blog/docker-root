//mongeez formatted javascript
//changeset system:v1_1
db.createCollection("user", { capped : true, autoIndexId : true });
db.user.createIndex({username: 1}, { unique: true });
db.user.insert({username: "admin", password: "admin"});

db.createCollection("dockerhost", { capped : true, autoIndexId : true });
db.dockerhost.createIndex({host: 1}, { unique: true });
db.dockerhost.insert({name: "arq-root", protocol: "tcp", host: "192.168.99.100", port: "2376", tls: '1'});
db.dockerhost.insert({name: "default", protocol: "tcp", host: "192.168.99.101", port: "2376", tls: '1'});
db.dockerhost.insert({name: "ubuntu18system", protocol: "tcp", host: "localhost", port: "3376", tls: '0'});