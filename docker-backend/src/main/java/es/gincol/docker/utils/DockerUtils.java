package es.gincol.docker.utils;

import com.github.dockerjava.api.DockerClient;
import com.github.dockerjava.api.command.DockerCmdExecFactory;
import com.github.dockerjava.core.DefaultDockerClientConfig;
import com.github.dockerjava.core.DockerClientBuilder;
import com.github.dockerjava.jaxrs.JerseyDockerCmdExecFactory;

public class DockerUtils {

	public static DockerClient getDockerClient(String host, String tlsVerify) {
		DefaultDockerClientConfig config = getDockerClientConfig(host, tlsVerify);
		DockerCmdExecFactory factory = getFactory();
		DockerClient dockerClient = DockerClientBuilder.getInstance(config)
				.withDockerCmdExecFactory(factory)
				.build();
		return dockerClient;
	}
	
	public static DefaultDockerClientConfig getDockerClientConfig(String host, String tlsVerify) {
		DefaultDockerClientConfig config
		  = DefaultDockerClientConfig.createDefaultConfigBuilder()		   
		    .withDockerTlsVerify(tlsVerify)
		    .withDockerHost(host).build();
		return config;
	}
	
	public static DockerCmdExecFactory getFactory() {
		DockerCmdExecFactory dockerCmdExecFactory = new JerseyDockerCmdExecFactory()
				  .withReadTimeout(1000)
				  .withConnectTimeout(1000)
				  .withMaxTotalConnections(100)
				  .withMaxPerRouteConnections(10);
		return dockerCmdExecFactory;
	}
	
	public static String getHost(String protocol, String host, String port) {
		return new StringBuilder(protocol)
				.append("://")
				.append(host)
				.append(":")
				.append(port)
			.toString();
	}
}
