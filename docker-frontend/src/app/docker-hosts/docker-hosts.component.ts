import { Component, OnInit } from '@angular/core';
import { DockerHostService } from '../services/docker-host.service';
import { Dockerhost } from '../model/dockerhost';

@Component({
  selector: 'app-docker-hosts',
  templateUrl: './docker-hosts.component.html',
  styleUrls: ['./docker-hosts.component.css']
})
export class DockerHostsComponent implements OnInit {

  dockerHosts: Dockerhost[];
  selectedDockerHost: Dockerhost;

  constructor(private dockerhostservice: DockerHostService) { }

  ngOnInit() {
    this.dockerhostservice.getDockerHosts()
      .subscribe(data => {
        this.dockerHosts = data;
      });

    this.dockerhostservice.dockerHostSelected.subscribe(
      (dockerHost: Dockerhost) => {
        this.selectedDockerHost = dockerHost;
      }
    );
  }

}
