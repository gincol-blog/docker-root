package es.gincol.docker.repository;

import java.util.List;

import org.springframework.data.mongodb.repository.MongoRepository;
import org.springframework.stereotype.Repository;

import es.gincol.docker.model.DockerHostApp;

@Repository
public interface DockerHostRepository extends MongoRepository<DockerHostApp, String> {
	
	public DockerHostApp findByHost(String host);
    public List<DockerHostApp> findAll();
}
