import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { FormsModule } from '@angular/forms';
import { HttpModule } from '@angular/http';
import { HttpClientModule } from '@angular/common/http';

import { AppComponent } from './app.component';
import { HeaderComponent } from './header/header.component';
import { DockerHostsComponent } from './docker-hosts/docker-hosts.component';
import { DockerHostDetailComponent } from './docker-hosts/docker-host-detail/docker-host-detail.component';
import { ImagesComponent } from './images/images.component';
import { ImageDetailComponent } from './images/image-detail/image-detail.component';
import { ContainersComponent } from './containers/containers.component';
import { ContainerDetailComponent } from './containers/container-detail/container-detail.component';
import { VolumesComponent } from './volumes/volumes.component';
import { VolumeDetailComponent } from './volumes/volume-detail/volume-detail.component';
import { NetworksComponent } from './networks/networks.component';
import { NetworkDetailComponent } from './networks/network-detail/network-detail.component';
import { DockerHostService } from './services/docker-host.service';
import { DockerHostItemComponent } from './docker-hosts/docker-host-item/docker-host-item.component';
import { FooterComponent } from './footer/footer.component';
import { Routes, RouterModule } from '@angular/router';
import { InicioComponent } from './inicio/inicio.component';
import { LoginComponent } from './login/login.component';
import { SignupComponent } from './auth/signup/signup.component';
import { SigninComponent } from './auth/signin/signin.component';
import { UsersService } from './services/users.service';
import { LoginService } from './services/login.service';

const appRoutes: Routes = [
  { path: '', component: InicioComponent },
  { path: 'home', component: InicioComponent },
  { path: 'login', component: LoginComponent },
  { path: 'dockerhost', component: DockerHostsComponent },
  { path: 'dockerhost/:id', component: DockerHostDetailComponent },
  { path: 'dockerhostdetail', component: DockerHostDetailComponent },
  { path: 'images', component: ImagesComponent },
  { path: 'container', component: ContainersComponent },
  { path: 'volumnes', component: VolumesComponent },
  { path: 'networks', component: NetworksComponent },
  { path: 'signup', component: SignupComponent }
];

@NgModule({
  declarations: [
    AppComponent,
    HeaderComponent,
    DockerHostsComponent,
    DockerHostDetailComponent,
    ImagesComponent,
    ContainersComponent,
    ContainerDetailComponent,
    ImageDetailComponent,
    VolumesComponent,
    VolumeDetailComponent,
    NetworksComponent,
    NetworkDetailComponent,
    DockerHostItemComponent,
    FooterComponent,
    InicioComponent,
    LoginComponent,
    SignupComponent,
    SigninComponent
  ],
  imports: [
    BrowserModule,
    FormsModule,
    HttpModule,
    HttpClientModule,
    RouterModule.forRoot(appRoutes)
  ],
  providers: [DockerHostService, LoginService, UsersService],
  bootstrap: [AppComponent]
})
export class AppModule { }
