package es.gincol.docker.controller;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

import io.swagger.annotations.ApiOperation;

@CrossOrigin(origins = "*", allowedHeaders = "*")
@RestController
@RequestMapping("/monitor")
public class MonitorController {

	private static final Logger log = LoggerFactory.getLogger(MonitorController.class);

	@ApiOperation(value="Prueba de app preparada")
	@GetMapping(value = "/readiness")
	public @ResponseBody String readiness() throws Exception {
		log.info("Acceso al servicio 'readiness'");
		return new StringBuilder().append("OK").toString();
	}

	@ApiOperation(value="Prueba de app activa")
	@GetMapping(value = "/liveness")
	public @ResponseBody String liveness() throws Exception {
		log.info("Acceso al servicio 'liveness'");
		return new StringBuilder().append("OK").toString();
	}
	
}