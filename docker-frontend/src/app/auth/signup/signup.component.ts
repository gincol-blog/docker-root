import { Component, OnInit } from '@angular/core';
import { NgForm } from '@angular/forms';
import { UsersService } from '../../services/users.service';
import { User } from '../../model/user';
import { Router } from '@angular/router';
import { LoginService } from '../../services/login.service';

@Component({
  selector: 'app-signup',
  templateUrl: './signup.component.html',
  styleUrls: ['./signup.component.css']
})
export class SignupComponent implements OnInit {

  showError = false;
  error: string;
  allowLogin = false;

  constructor(private router: Router, private usersService: UsersService, private loginservice: LoginService) { }

  ngOnInit() {
  }

  onSignup(form: NgForm){
    const username = form.value.username;
    const password = form.value.password;
    const user: User = new User(username, password);
    this.usersService.signUp(user)
      .subscribe(data => { 
        console.log(data);
        if (data.tipo != "Error") {
          this.loginservice.logged = true;
          this.router.navigate(['/dockerhost']);
        } else {
          this.loginservice.logged = false;
          this.showError = true;
          this.error = data.texto;
        }
      });	 
      
  }
  
}
