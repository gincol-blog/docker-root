package es.gincol.docker.repository;

import java.util.List;

import org.springframework.data.mongodb.repository.MongoRepository;
import org.springframework.stereotype.Repository;

import es.gincol.docker.model.User;

@Repository
public interface UserRepository extends MongoRepository<User, String> {
	public User findByUsername(String username);
	public User findByUsernameAndPassword(String username, String password);
    public List<User> findAll();
}
