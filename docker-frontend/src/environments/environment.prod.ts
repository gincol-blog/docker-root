export const environment = {
  production: true,
  envName: 'prod',
  dockerHostUrl: 'https://gincol.blog.com/dockerhost',
  loginUrl: 'http://gincol.blog.com/login',
  addUserUrl: 'http://gincol.blog.com/users/addShort'
};
