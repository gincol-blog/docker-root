import { Component, OnInit } from '@angular/core';
import { DockerHostService } from '../services/docker-host.service';
import { Dockerhost } from '../model/dockerhost';
import { LoginService } from '../services/login.service';
import { ActivatedRoute, Router } from '@angular/router';

@Component({
  selector: 'app-images',
  templateUrl: './images.component.html',
  styleUrls: ['./images.component.css']
})
export class ImagesComponent implements OnInit {
  
  dockerHosts: Dockerhost[];
  dockerHostSelected: String;

  constructor(private dockerhostservice: DockerHostService, private loginservice: LoginService, private route: ActivatedRoute, private router: Router) {     
  }

  ngOnInit() {
    if (this.loginservice.loggedIn()){
      this.dockerhostservice.getDockerHosts()
        .subscribe(data => {
          this.dockerHosts = data;
        });
    } else {
      this.router.navigateByUrl("/login");
    }
    
  }

  onSelection(event: any){
    this.dockerHostSelected = event.target.value;
    console.log(event.target.value);
  }

  showImages(){
    console.log(this.dockerHostSelected);
  }
}
