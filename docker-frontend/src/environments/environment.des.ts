export const environment = {
  production: false,
  envName: 'des',
  dockerHostUrl: 'http://localhost:8080/dockerhost',
  loginUrl: 'http://localhost:8080/login',
  addUserUrl: 'http://localhost:8080/users/addShort'
};
